const data = [
     {
          singer : "am-c",
          songs: [
              "gund", "huurhun baigaach", "perfect blue" ,  "sanasan baina", "uhsen baas" ,"yag enchee" ,"zurhendee sonsooroi" ,"yu husej baina" ,"habibi"  ,"starry night" ,"zombie"
          ],
          link:"amc.html",
          photo:"amc.jpg",
          cover: "amcCover.png",
          name: "Аm-C",
          sub : 112014,
          cat : "Соло артист",
          duu : [
               "Гүнд", "Хөөрхөн байгаач", "Perfect Blue", "Санасан байна", "Үхсэн баас", "Яг энчээ", "Зүрхэндээ сонсоорой", "Юу хүсэж байна", "Habibi","Starry Night", "Zombie"
          ],
          mp3: [
               "Gund.mp3", "Huurhun Baigaach.mp3", "Perfect Blue.mp3", "Sanasan Baina.mp3",  "Uhsen Bass.mp3","Yag Enchee.mp3", "Zurhendee Sonsooroi.mp3", "Yu Husej Baina.mp3", "Habibi.mp3", 
                "STARRY NIGHT.mp3","Zombie.mp3"
          ],
          songdur: [
               "03:58", "03:06", "04:48", "03:21", "03:03", "03:09", "03:07", "03:09", "04:01", "03:32","03:09",
          ],
          fb : "https://www.facebook.com/amc.laav",
          ig : "https://www.instagram.com/am_ccc/?hl=en",
          yt : "https://www.youtube.com/channel/UCWF6J9vWH2e9NXCr5NMDUtw",
     },
     {
          singer : "vandebo",
          songs: [
               "too deep", "chamin yum", "galt tereg", "hair", "huuhdiin 100", "namaig too", "sain bodoj uzsen uu", "unana", "haru haru", "ayo", "18 savage", "zogsohgui"
          ],
          link:"vandebo.html",
          photo: "vandebo.jpg",
          cover :"vandeboCover.png",
          name:"Vandebo",
          sub : 105562,
          cat : "Хамтлаг",
          duu : [
               "Too Deep", "Чамин юм", "Галт тэрэг", "Хайр", "Хүүхдийн 100", "Намайг тоо", "Сайн бодож үзсэн үү", "Унана", "Haru Haru","Ayo", "18 Savage","Зогсохгүй",
          ],
          mp3: [
               "Too Deep.mp3", "Chamin Yum.mp3", "Galt Tereg.mp3", "Hair.mp3",  "HUUHDIIN 100.mp3", "Namaig Too.mp3", "Sain Bodoj Uzsen Uu.mp3", "Unana.mp3", "Haru Haru.mp3", 
               "AYO.mp3", "18 Savage.mp3", "Zogsohgui.mp3"
          ],
          songdur:[
               "03:43", "02:59", "02:55", "03:17", "03:49", "02:51", "03:39", "02:59", "03:39", "04:46", "03:15", "03:56",
          ],
          fb : "https://www.facebook.com/officialvandebo",
          ig : "https://www.instagram.com/officialvandebo/?hl=en",
          yt: "https://www.youtube.com/Vandebo",
     },
     {
          singer : "sash",
          songs : [
               "6", "bayartai", "chi", "erh duraaraa galzuu", "goo", "nisvanis", "rari", "close to you", "tsonhon door", "mungu", "zaisan"
          ],
          link : "sash.html",
          photo: "sash.jpg",
          cover: "sashCover.png",
          name:"Sash",
          sub : 10650,
          cat : "Соло артист",
          duu : [
               "6", "Баяртай", "Чи", "Эрх дураараа галзуу", "Гоо", "Нисванис", "Rari", "Close to you", "Цонхон доор","Мөнгө", "Зайсан",
          ],
          mp3: [
               "6.mp3", "Bayartai.mp3", "Chi.mp3", "Erh, duraaraa, galzuu.mp3",  "Goo.mp3", "NISVANIS.mp3", "RARI.mp3", "close to YOU.mp3", "Tsonhon door.mp3",  "MUNGU.mp3",
               "ZAISAN.mp3",
          ],
          songdur:[
               "02:09", "02:32", "02:44", "02:32", "03:40", "03:33", "02:43", "01:44", "01:43","02:48", "02:44",
          ],
          fb : "https://www.facebook.com/sashmusic",
          ig : "https://www.instagram.com/haluunpizza/?hl=en",
          yt : "https://www.youtube.com/channel/UCoWRFzQleLwAt_wdNDLxikg",

     },
     {
          singer : "choijoo",
          songs :[
               "havriin gants ulias", "salhind shivneh ugs", "chamd uldeey", "hurch chadahgui", "sain bisx", "miniih", "margaash borootoi", "ene duug min duusahaas umnu", "ahin dahin durlamaar", "1001 tsetsegs"
          ],
          link :"choijoo.html",
          photo: "choijoo2.jpg",
          name :"Чойжоо",
          cover: "choijooCover.png",
          sub : 24972,
          cat : "Соло артист",
          duu : [
               "Хаврын ганц улиас", "Салхинд шивнэх үгс", "Чамд үлдээе", "Хүрч чадахгүй", "Sain bisx", "Минийх", "Маргааш бороотой", "Энэ дууг минь дуусахаас өмнө", "Ахин дахин дурламаар","1001 Цэцэгс",
          ],
          mp3: [
               "Havriin gants ulias.mp3", "Salhind shivneh ug.mp3", "Chamd Uldeey.mp3", "Hurch chadahgui.mp3",  "Sain bisx.mp3", "Miniih.mp3", "Margaash borootoi.mp3", "Ene duug mine duusahaas umnu.mp3", "Ahin dahin durlamaar.mp3", 
               "Thousand One Flowers.mp3",
          ],
          songdur:[
               "03:55", "04:07", "04:34", "04:22", "03:21", "03:55", "03:31", "03:21", "04:07", "03:54", 
          ],
          fb : "https://www.facebook.com/choi.joo.7",
          ig : "https://www.instagram.com/choi_joo.mongolia/?hl=en",
          yt : "https://www.youtube.com/channel/UCKH5OVAgjgAL_CRq__9olNA",

     },
     {
          singer : "ginjin",
          songs : [
               "so cold", "arasso", "ene much", "ganbei", "missed call", "unuudur", "atm", "birthday", "biye daa", "boroo"
          ],
          link: "ginjin.html",
          photo: "ginjin.jpg",
          cover: "ginjinCover.png",
          name :"Гинжин",
          sub : 106340,
          cat : "Соло артист",
          duu : [
               "So Cold", "Arasso", "Энэ мөч", "Ganbei", "Missed call", "Өнөөдөр", "ATM", "Birthday", "Биеэ Даа", "Бороо"
          ],
          mp3: [
               "So Cold.mp3", "Arasso.mp3", "Ene Much.mp3", "Ganbei.mp3",  "Missed Call.mp3", "UNUUDUR.mp3", "ATM.mp3", "Birthday.mp3", "Biye Daa.mp3", 
               "Boroo.mp3",
          ],
          songdur:[
               "02:59", "03:50", "03:25", "03:21", "03:19", "04:08", "03:00", "03:05", "02:32", "03:01", 
          ],
          fb : "https://www.facebook.com/ginjinsor",
          ig : "https://www.instagram.com/ginjinsor/?hl=en",
          yt : "https://www.youtube.com/ginjin",
     },
     {
          singer : "nene",
          songs :[
               "unendee", "chas tas", "too shy", "sugar mama", "bon o bon", "tko", "one shot", "us tsaital hamt"
          ],
          link: "nene.html",
          photo: "nene.jpg",
          cover: "neneCover.png",
          name :"Nene",
          sub : 64730,
          cat : "Соло артист",
          duu :[
               "Үнэндээ", "Час Тас", "Too shy", "Sugar Mama", "Bon'o Bon", "T.K.O", "One Shot", "Үс Цайтал Хамт"
          ],
          mp3: [
               "Unendee.mp3", "Chas Tas.mp3", "TOO SHY.mp3", "Sugar Mama.mp3", "Bon o Bon.mp3", "TKO.mp3", "One Shot.mp3", "Us tsaital hamt.mp3",
          ],
          songdur:[
               "03:51", "03:14", "03:00", "02:38", "03:15", "03:15", "04:35", "03:16", 
          ],
          fb : "https://www.facebook.com/ArtistNeneOfficial/",
          ig : "https://www.instagram.com/haluunmaruujin/",
          yt : "https://www.youtube.com/channel/UCYQw8fuo5_wGgegFbweiM7Q",
     },
     {
          singer : "rokit bay",
          songs : [
               "evderhii hun", "sahilga sul", "neg udriin haan", "chutguriin PR", "bid hoyr", "hyazgaargui", "bor arist gaamp", "oriloon", "hooson zai", "zugaatai bailaa","uulen domog"
          ],
          link : "rokitbay.html",
          photo: "rokitbay2.jpg",
          cover: "rokitbayCover.png",
          name:"Rokit Bay",
          sub : 88756,
          cat : "Соло артист",
          duu : [
               "Эвдэрхий Хүн", "Сахилга Сул", "Нэг Өдрийн Хаан", "Чөтгөрийн PR", "Бид Хоёр", "Хязгааргүй", "Бор арьст гаамп", "Орилоон", "Хоосон зай", "Зугаатай байлаа","Үүлэн домог",
          ],
          mp3:[
               "Эвдэрхий хүн.mp3", "Sahilga sul.mp3", "Neg Udriin Haan.mp3","Chutguriin PR.mp3","Bid 2.mp3","HYZGAARGUI.mp3", "Бор арьст Гаамп.mp3","Орилоон.mp3", "Hooson zai.mp3", "Зугаатай байлаа.mp3","Uulen Domog.mp3",
          ],
          songdur :[
               "04:58", "03:17", "04:58", "03:13", "04:51", "03:31", "03:26", "04:02", "03:51", "03:13", "04:44",
          ],
          fb : "https://www.facebook.com/Rokitbayofficial/",
          ig : "https://www.instagram.com/bay_odon/?hl=en",
          yt : "https://www.youtube.com/channel/UCIgJUusl6itFNLTBy5W8ecw",
     },
     {
          singer : "nisvanis",
          songs :[
               "muu zurshil", "ulaan heruulch", "az jargaltai tugsdug", "nergui duu", "esreg utgaar n", "minii nirvana", "tsag hugatsaa bugdiig emchilne", "setgel hudulnu", "nisdeg tavag", "hezee negen tsagt bid nisej magadgui", "zugaatai bailaa",
          ],
          link: "nisvanis.html",
          photo: "nisvanis.jpg",
          cover: "nisvanisCover.png",
          name: "Нисванис",
          sub : 909,
          cat : "Хамтлаг",
          duu :[
               "Муу зуршил", "Улаан хэрүүлч", "Аз жаргалтай төгсдөг", "Нэргүй дуу", "Эсрэг утгаар нь", "Миний нирвана", "Цаг хугацаа бүгдийг эмчилнэ", "Сэтгэл хөдөлнө", "Нисдэг таваг", "Хэзээ нэгэн цагт бид нисэж магадүй", "Зугаатай байлаа", 
          ],
          mp3:[
               "Муу Зуршил.mp3", "Улаан хэрүүлч.mp3", "Az Jargaltai Togsdog.mp3", "Нэргүй дуу.mp3", "Эсрэг утгаар нь.mp3", "Minii NIRVANA.mp3", "Цаг Хугацаа Бүгдийг Эмчилнэ.mp3", "Setgel hudulnu.mp3", "Nisdeg Tavag.mp3", "Hezee Negen Tsagt Bid Nisej Magadgui.mp3", "Зугаатай байлаа.mp3"
          ],
          songdur :[
               "04:19", "03:10", "05:33", "02:36", "04:03", "05:13", "04:08", "04:57", "03:11", "04:54", "03:13", 
          ],
          fb : "https://www.facebook.com/Nisvanis",
          ig : "https://www.instagram.com/nisvanis_offical/",
          yt : "https://www.youtube.com/channel/UCpIFwbc5DQSg18Wts7kVcVA",
     },
     {
          singer : "bold",
          songs : [
               "one shot", "yaj chi uurchilvuu", "chi nadad durlaasai", "hairtai huniihee gar luu guigeerei", "ayo", "nud chin hair haruulna", "bidnii amidardag ertunts", "udaanaar", "chamd sonsogdoj baina uu","daam","uulen domog",
          ],
          link :"bold.html",
          photo: "bold.jpg",
          cover: "boldCover.png",
          name : "Болд",
          sub : 124692,
          cat : "Соло артист",
          duu : [
               "One Shot", "Яаж чи өөрчилвөө", "Чи надад дурлаасай", "Хайртай хүнийхээ гар луу гүйгээрэй", "Ayo", "Нүд чинь хайр харуулна", "Бидний амьдардаг ертөнц", "Удаанаар", "Чамд сонсогдож байна уу","ДААМ","Үүлэн Домог",
          ],
          mp3:[
               "One Shot.mp3", "Яаж чи өөрчилвөө.mp3", "Chi Nadad Durlaasai.mp3", "Hairtai Huniihee Gar Luu Guigeerei.mp3", "AYO.mp3", "Nud Chin Hair Haruulna.mp3", "Bidnii amidardag ertunts.mp3", "Udaanaar.mp3","Chamd Sonsogdoj Baina Uu.mp3", "DAAM.mp3", "Uulen Domog.mp3"
          ],
          songdur :[
               "04:35", "04:13", "03:25", "03:52", "04:46", "04:03", "03:23", "04:02", "03:49", "03:39", "04:44",
          ],
          fb : "https://www.facebook.com/Bold.Musician/",
          ig : "https://www.instagram.com/bold.dorjsuren/?hl=en",
          yt : "https://www.youtube.com/channel/UCrEwWFYZ0uvQtDCwCbBrV2w",
     },
     {
          singer: "thunder",
          songs :[
               "neg l uhne", "baigaagaar min huleen avah uu", "dandaa daardag ohin", "oyutan", "bi chinii naiz zaluu baij boloh uu", "er hun shg er hun bai", "ex bolmoorgui baina", "cool aav", "duurge", 
               "gantshan shaltgaan", "i am a man", "mommy",
          ],
          link :"thunder.html",
          photo: "thunder.jpg",
          cover: "thunderCover.png",
          name: "Thunder",
          sub : 227801,
          cat : "Соло артист",
          duu :[
               "Нэг л үхнэ", "Байгаагаар минь хүлээн авах уу", "Дандаа даардаг охин", "Оюутан", "Би чиний найз залуу байж болох уу", "Эр хүн шиг эр хүн бай", "EX болмооргүй байна", "Cool Aav", "Дүүргэ", 
               "Ганцхан шалтгаан", "I am a Man", "M.O.M.M.Y",
          ],
          mp3: [
               "Neg l Uhne.mp3", "Baigaagaar min huleen avah uu.mp3", "Dandaa Daardag Ohin.mp3", "Oyutan.mp3",  "Bi Chinii Naiz Zaluu Baij Bolhuu.mp3"," Er hun shig er hun bai.mp3", "Ex Bolmoorgui Baina.mp3", "Cool Aav.mp3", "Duurge.mp3", 
               "Gantshan Shaltgaan.mp3", "I AM MAN.mp3","Mommy.mp3",
          ],
          songdur: [
               "04:05", "02:48", "03:14", "05:01", "03:56", "03:25", "03:04", "02:41", "03:23", "03:10","04:12","03:12",
          ],
          fb : "https://www.facebook.com/thunderzrapper",
          ig : "https://www.instagram.com/thunderzrapper/?hl=en",
          yt : "https://www.youtube.com/channel/UCSYAYTS8OwlknJMOmKXSB9g",
     }
]

const playList = [
     {
          name: "Мөнх-Оргил",
          user :  "Orgil",
          photo : "orgilPlaylist.jpeg",
          songs : [
               "haru haru", "namaig too", "huuhdiin 100", "erh duraaraa galzuu", "ahin dahin durlamaar", "sain bisx", "chas tas", "too shy", "hyazgaargui", "muu zurshil", "bidnii amidardag ertunts",
          ],
          link : "O-Mix.html",
          cover : "orgilCover.png",
          fb : "https://www.facebook.com/munkhorgil.erdenebold",
          ig : "https://www.instagram.com/_orgil6m_/", 
          mail : "https://mail.google.com/mail/?view=cm&fs=1&to=munkhorgil0602@gmail.com&su=Subject&body=&bcc=munkhorgil0602@gmail.com",
     },
     {
          name: "Ням-Эрдэнэ",
          user : "Nyamka",
          photo: "nyamkaPlaylist.png",
          songs: [
               "6","boroo","unana", "haru haru", "namaig too", "yu husej baina", "sanasan baina", "dandaa daardag ohin", "evderhii hun", "us tsaital hamt",
          ],
          link : "N-Mix.html",
          cover: "nyamkaCover.png",
          fb : "https://www.facebook.com/erdene.nyamkaa",
          ig : "https://www.instagram.com/__nyamkaa__/", 
          mail : "https://mail.google.com/mail/?view=cm&fs=1&to=nyamk.nest@gmail.com&su=Subject&body=&bcc=nyamk.nest@gmail.com",
     },
     {
          name: "Эрмүүн",
          user : "Ermuun",
          photo: "ermuunPlaylist.jpg",
          songs: [
               "us tsaital hamt", "hyazgaargui", "perfect blue", "boroo", "hairtai huniihee gar luu guigeerei", "neg l uhne", "chas tas", "sugar mama", "tko", "too deep",
          ],
          link : "E-Mix.html",
          cover : "ermuunCover.png",
          fb : "https://www.facebook.com/Ermuunn/",
          ig: "https://www.instagram.com/_ermuun_/", 
          mail: "https://mail.google.com/mail/?view=cm&fs=1&to=ermuunb2004@gmail.com&su=Subject&body=&bcc=ermuunb2004@gmail.com",
     },
]


const chart = [
     {
     songs: [
          "gund", "unana", "erh duraaraa galzuu", "hurch chadahgui","boroo", "chas tas", "bor arist gaamp", "muu zurshil", "yaj chi uurchilvuu", "duurge",
     ],
     singer:[
          "am-c", "vandebo", "sash", "choijoo", "ginjin", "nene", "rokit bay", "nisvanis", "bold", "thunder",
     ]
     }
]

const newSongs = {
     songs: [
          "tsonhon door", "us tsaital hamt", "one shot", "hyazgaargui", "nergui duu", "haru haru", "perfect blue", "sain bisx", "so cold", "duurge"
     ],
     singer:[
          "sash", "nene", "bold", "rokit bay", "nisvanis", "vandebo", "am-c", "choijoo", "ginjin", "thunder",
     ]
}